@import url('https://fonts.googleapis.com/css2?family=Lobster&family=PT+Sans:wght@700&family=Rajdhani:wght@300&display=swap');

font-family: 'Lobster', cursive; /* Título de área */
font-family: 'PT Sans', sans-serif; /* Menu e Link */
font-family: 'Rajdhani', sans-serif; /* Caixa de texto */